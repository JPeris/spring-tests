# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Repository for some tests to learn/practise some capabilities of Spring 

### How do I get set up? ###

* You should have a maven installation or an IDE with maven bundled
* There's no need for special configuration
* The dependencies will be downloaded from maven local repository
* mvn clean spring-boot:run
* mvn clean test

### Contribution guidelines ###

* Any feedback is always welcome!

### Who do I talk to? ###

* Contact to javierperis@gmail.com for any suggestion