package controller;

import exceptions.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import repository.UserRepository;

import java.util.Collection;

/**
 * Created by javier on 30-Dec-16.
 */

@Controller
public class UserController {

    private final UserRepository userRepository;

    @Autowired
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @ExceptionHandler(UserNotFoundException.class)
    @RequestMapping(value="/user/{login}", method=RequestMethod.GET)
    @ResponseBody
    public User getUser(@PathVariable String login)  {
        User user = userRepository.findUser(login);
        if (null == user) throw new UserNotFoundException();
        return user;
    }

    @RequestMapping(value="/users/", method= RequestMethod.GET)
    public @ResponseBody
    Collection<User> getUsers(){
        return userRepository.getUsers();
    }

}
