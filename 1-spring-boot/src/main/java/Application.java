/**
 * Created by javier on 28-Dec-16.
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import repository.UserRepository;
import repository.UserRepositoryImpl;

@Configuration
@ComponentScan("controller")
@EnableAutoConfiguration
public class Application {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public UserRepository userRepository(){
        return new UserRepositoryImpl();
    }
}
