package repository;

import controller.User;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by javier on 30-Dec-16.
 */
public class UserRepositoryImpl implements UserRepository {

    private Set<User> users = Collections.newSetFromMap(new ConcurrentHashMap<User, Boolean>());

    @Override
    public boolean save(User user) {
        return users.add(user);
    }

    @Override
    public Collection<User> getUsers() {
        return users;
    }

    @Override
    public void deleteAllInBatch() {
        users.clear();
    }

    @Override
    public User findUser(String login) {
        for (User user : users) {
            if (user.getLogin().equals(login)) {
                return user;
            }
        }
        return null;
    }
}
