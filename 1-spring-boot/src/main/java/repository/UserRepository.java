package repository;

import controller.User;

import java.util.Collection;

/**
 * Created by javier on 30-Dec-16.
 */
public interface UserRepository {

    Collection<User> getUsers();

    void deleteAllInBatch();

    boolean save(User user);

    User findUser(String login);
}
