package exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by javier on 31-Dec-16.
 */
@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="No such User")
public class UserNotFoundException extends RuntimeException{

    private static final long serialVersionUID = 1L;

}
