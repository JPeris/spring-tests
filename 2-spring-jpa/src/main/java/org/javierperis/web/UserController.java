package org.javierperis.web;

import org.javierperis.domain.User;
import org.javierperis.web.exceptions.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.javierperis.repository.UserRepository;

import java.util.Collection;

/**
 * Created by javier on 30-Dec-16.
 */

@Controller
public class UserController {

    private final UserRepository userRepository;

    @Autowired
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/user/{login}")
    @ResponseBody
    public ResponseEntity<User> getUser(@PathVariable String login)  {
        return userRepository.findOneByLogin(login)
                .map( user -> new ResponseEntity<>(user, HttpStatus.OK))
                .orElseThrow(UserNotFoundException::new);
    }

    @GetMapping("/users/")
    public @ResponseBody
    Collection<User> getUsers(){
        return userRepository.findAll();
    }

}
