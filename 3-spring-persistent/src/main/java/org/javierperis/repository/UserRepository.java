package org.javierperis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.javierperis.domain.User;

import java.util.Optional;

/**
 * Created by javier on 30-Dec-16.
 */
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findOneByLogin(String login);
}
